<?	session_start();
	include_once ('_db.php');
	if ( !class_exists('_contact') ) {
		class _contact extends _db {
	
			public $table = 'contact';
			
			public function get_fio($id) {
				$db = new _db();
				$result = self::$dbh -> prepare("SELECT `fio` FROM `".$this->DBName."`.`".$this->table."` WHERE id = :id");
				$result -> bindValue('id', $id, PDO::PARAM_INT);
				$result -> execute();
				foreach ( $result->fetchAll() as $row ) {
					return $row['fio'];
				}
			}
			
			public function manager_update($id, $state) {
				$manager = new _manager();
				$result = self::$dbh -> prepare("UPDATE `".$this->DBName."`.`".$this->table."` SET manager = :manager WHERE id = :id");
				$result -> bindValue('manager', (int)$manager -> get_id($state), PDO::PARAM_INT);
				$result -> bindValue('id', (int)$id, PDO::PARAM_INT);
				$result -> execute();
			}
			
			public function get_contact() {
				$manager = new _manager();
				$task = new _task();
				$deal = new _deal();
				if ( ($_SESSION['rang'] == '999') || ($_SESSION['rang'] == '555') ) {
					$result = self::$dbh->prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."`;");
				} else {
					$result = self::$dbh->prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE manager = :manager ;");
					$result -> bindValue('manager', $_SESSION['log'], PDO::PARAM_INT);
				}
				$result->execute();
				$i = 1;
				echo "<table border=0 width='100%' cellpadding=0 cellspacing=0>
						<thead>
							<tr>
								<th width=30></td>
								<th width=130><strong>Имя Фамилия</strong></td>
								<th width=130><strong>Организация</strong></td>
								<th width=130><strong>Должность</strong></td>
								<th width=130><strong>Ответственный</strong></td>
								<th width=180><strong>Статус сделки</strong></td>
							</tr>
						</thead>";
				foreach ($result->fetchAll() as $row) {
					$id = $task -> contact( $row['id'] );
					echo "<tr id='".$row['id']."'><td width=30><input type='checkbox' class='deal' id='".$row['id']."'/></td><td width=130>";
					echo $row['fio'];
					echo "</td><td width=130>";
					echo $row['company'];
					echo "</td><td width=130>";
					echo $row['state'];
					echo "</td><td width=130>";
					echo $manager -> get_fio($row['manager']);
					echo "</td><td width=180 class='deal_status'><span class='status_".$deal -> get_status( $row['id'] )."'>";
					echo $deal -> unconvert_status( $deal -> get_status( $row['id'] ) );
					echo "</span></td><td style='display: none'>";
					echo $row['icq'];
					echo "</td><td style='display: none' class='task_id'>";
					echo $task -> contact( $row['id'] );
					echo "</td><td style='display: none' class='task_date'>";
					echo $task -> date_task( $id );
					echo "</td><td style='display: none' class='task_status'>";
					echo $task -> get_status( $id );
					echo "</td><td style='display: none'>";
					echo $row['tags'];
					echo "</td></tr>";
				}
				echo "</table>";
			}
			
			public function get_id($fio) {
				$result = self::$dbh->prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."`;");
				$result -> execute();
				foreach ($result->fetchAll() as $row) {
					if ($row[fio] == $fio) { return $row['id']; };
				}
			}
			
			public function get_company($id) {
				$result=self::$dbh->prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE company = :id");
				$result->bindValue('id', $id, PDO::PARAM_INT);
				$result->execute();
				foreach ($result->fetchAll() as $row) {
					echo $row['fio'];
				}
			}
			
			public function list_contact() {
				$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."`;");
				$result -> execute();
				foreach ($result -> fetchAll() as $row) {
					echo "<option value='";
					echo $row['fio'];
					echo ", ";
					echo $row['company'];
					echo "'></option>";
				}
			}
			
			public function set_contact($fio, $man, $company, $state, $phone, $mail, $web, $icq, $address, $tags) {
				$manager = new _manager();
				$result = self::$dbh -> prepare("INSERT INTO `".$this->DBName."`.`".$this->table."` (id, fio, manager, company, state, phone, mail, web, icq, address, tags) VALUES (null, :fio, :manager, :company, :state, :phone, :mail, :web, :icq, :address, :tags)");
				$result -> bindValue('fio', (string)$fio, PDO::PARAM_STR);
				$result -> bindValue('manager', (int)$manager->get_id($man), PDO::PARAM_INT);
				$result -> bindValue('company', (string)$company, PDO::PARAM_STR);
				$result -> bindValue('state', (string)$state, PDO::PARAM_STR);
				$result -> bindValue('phone', (string)$phone, PDO::PARAM_STR);
				$result -> bindValue('mail', (string)$mail, PDO::PARAM_STR);
				$result -> bindValue('web', (string)$web, PDO::PARAM_STR);
				$result -> bindValue('icq', (string)$icq, PDO::PARAM_STR);
				$result -> bindValue('address', (string)$address, PDO::PARAM_STR);
				$result -> bindValue('tags', (string)$tags, PDO::PARAM_STR);
				$result -> execute();
			}
			
			public function delete_contact($id) {
				$result = self::$dbh->prepare('DELETE FROM `'.$this->DBName.'`.`'.$this->table.'` WHERE id = :id');
				$result -> bindValue('id', $id, PDO::PARAM_STR);
				$result -> execute();
			}
			
			public function lastInsertId() { return self::$dbh -> lastInsertId(); }
		}
	}
?>