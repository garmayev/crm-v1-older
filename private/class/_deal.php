<?	session_start();
	include_once ('_db.php');
	class _deal extends _db {

		protected $table = 'deal';
		
		public function get_deal() {
			$contact = new _contact();
			$manager = new _manager();
			if ( ($_SESSION['rang'] == '999') || ($_SESSION['rang'] == '555') ) {
				$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."`;");
			} else {
				$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE manager = :manager;");
				$result -> bindValue('manager', $_SESSION['log'], PDO::PARAM_INT);
			}
			$result -> execute();
			echo "<table border=0 width=100% cellspacing=0 cellpadding=0>
						<thead>
							<tr>
								<th width=30></td>
								<th width=150><strong>Название сделки</strong></td>
								<th width=150><strong>Контакт</strong></td>
								<th width=150><strong>Организация</strong></td>
								<th width=150><strong>Статус сделки</strong></td>
								<th width=100><strong>Бюджет</strong></td>
							</tr>
						</thead>";
			foreach ($result->fetchAll() as $row) {
				echo "<tr id='".$row['id']."'>
						<td width=30>
							<input type='checkbox' class='deal' id='".$row['id']."'/>
						</td><td width=150>";
				echo $row['name'];
				echo "	</td><td width=150>";
				echo $contact -> get_fio($row['contact']);
				echo "</td><td width=150>";
				echo $row['company'];
				echo "</td><td width=150>";
				switch ($row['status']) {
					case 1:
						echo "<span class='status_".$row['status']."'>Первичный контакт</span>";
					break;
					case 2:
						echo "<span class='status_".$row['status']."'>Переговоры</span>";
					break;
					case 3:
						echo "<span class='status_".$row['status']."'>Принимают решение</span>";
					break;
					case 4:
						echo "<span class='status_".$row['status']."'>Согласование договора</span>";
					break;
					case 5:
						echo "<span class='status_".$row['status']."'>Успешно реализовано</span>";
					break;
					case 6:
						echo "<span class='status_".$row['status']."'>Закрыто и не реализовано</span>";
					break;
				};
				echo "</td><td width=100>";
				echo $row['money'];
				echo "</td><td style='display: none;' class='manager'>";
				echo $manager -> get_fio($row['manager']);
				echo "</td><td style='display: none;'>";
				echo $row['tags'];
				echo "</td><td style='display: none;'><span>";
				echo $row['date'];
				echo "</span></td></tr>";
			}
			echo "</table>";
		}
	
		public function get_name($id) {
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE id = :id;");
			$result -> bindValue('id', $id, PDO::PARAM_INT);
			$result -> execute();
			foreach ($result->fetchAll() as $row) {
				return $row['name'];
			}
		}
	
		public function get_company($id) {
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE id = :id;");
			$result -> bindValue('id', $id, PDO::PARAM_INT);
			$result -> execute();
			foreach ($result->fetchAll() as $row) {
				return $row['company'];
			}
		}
		
		public function get_contact($id) {
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE id = :id;");
			$result -> bindValue('id', $id, PDO::PARAM_INT);
			$result -> execute();
			foreach ($result->fetchAll() as $row) {
				return $row['contact'];
			}
		}
		
		public function convert_status($status) {
			switch ( $status ) {
				case "Первичный контакт":
					return (int)1;
				break;
				case "Переговоры":
					return (int)2;
				break;
				case "Принимают решение":
					return (int)3;
				break;
				case "Согласование договора":
					return (int)4;
				break;
				case "Успешно реализовано":
					return (int)5;
				break;
				case "Закрыто и нереализовано":
					return (int)6;
				break;
				default: 
					return "error";
				break; 
			}
		}
		
		public function unconvert_status($id) {
			switch ( $id ) {
				case 0:
					return (string)"Сделка отсутствует";
				break;
				case 1:
					return (string)"Первичный контакт";
				break;
				case 2:
					return (string)"Переговоры";
				break;
				case 3:
					return (string)"Принимают решение";
				break;
				case 4:
					return (string)"Согласование договора";
				break;
				case 5:
					return (string)"Успешно реализовано";
				break;
				case 6:
					return (string)"Закрыто и нереализовано";
				break;
			};
		}
		
		public function set_deal($company, $name, $status, $money, $man, $con, $tags) {
			$manager = new _manager();
			$contact = new _contact();
			$date = date('Y-m-d H:i:s');
			$result = self::$dbh -> prepare("INSERT INTO `".$this->DBName."`.`".$this->table."` (id, date, company, name, status, money, manager, contact, tags) VALUES (null, '$date', :company, :name, :status, :money, :manager, :contact, :tags); ");
			$result -> bindValue("company", (string)$company, PDO::PARAM_STR);
			$result -> bindValue("name", (string)$name, PDO::PARAM_STR);
			$result -> bindValue("status", $this -> convert_status($status), PDO::PARAM_INT);
			$result -> bindValue("money", (string)$money, PDO::PARAM_STR);
			$result -> bindValue("manager", (int)$manager -> get_id($man), PDO::PARAM_INT);
			$result -> bindValue("contact", (int)$contact -> get_id($con), PDO::PARAM_INT);
			$result -> bindValue("tags", (string)$tags, PDO::PARAM_STR);
			$result -> execute();
		}
		
		public function get_status ($id) {
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE contact = :contact;");
			$result -> bindValue('contact', $id, PDO::PARAM_INT);
			$result -> execute();
			foreach ($result->fetchAll() as $row) {
				if ( $row['status'] != null ) { return $row['status']; } else { return 0; }
			}
		}
		
		public function deal_and_cont() {
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."`");
			$result -> execute();
			foreach ($result->fetchAll() as $row) {
				echo "<option>";
				echo $row['name'];
				echo "</option>";
			}
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`contact`");
			$result -> execute();
			foreach ($result->fetchAll() as $row) {
				echo "<option>";
				echo $row['fio'];
				echo "</option>";
			}
		}
		
		public function deal_status_update($id, $state) {
			$result = self::$dbh -> prepare("UPDATE `".$this->DBName."`.`".$this->table."` SET status = :status WHERE id = :id");
			$result -> bindValue('status', $this->convert_status($state), PDO::PARAM_INT);
			$result -> bindValue('id', (int)$id, PDO::PARAM_INT);
			$result -> execute();
		}
		
		public function deal_manag_update($id, $state) {
			$manager = new _manager();
			$result = self::$dbh -> prepare("UPDATE `".$this->DBName."`.`".$this->table."` SET manager = :manager WHERE id = :id");
			$result -> bindValue('manager', (int)$manager -> get_id($state), PDO::PARAM_INT);
			$result -> bindValue('id', (int)$id, PDO::PARAM_INT);
			$result -> execute();
		}
		
		public function deal_del_update($id) {
			$result = self::$dbh -> prepare("DELETE FROM `".$this->DBName."`.`".$this->table."` WHERE id = :id");
			$result -> bindValue('id', $id, PDO::PARAM_INT);
			$result -> execute();
		}
		
		public function lastInsertId() { return self::$dbh -> lastInsertId(); }
	}
?>