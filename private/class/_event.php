<? 	session_start();
	include_once ('_db.php');
	class _event extends _db {

		public $table = 'event';
			
		private function print_event($res) {
			$manager = new _manager();
			$contact = new _contact();
			$deal = new _deal();
			$i = 1;
			echo "	<table border=0 width=100% cellpadding=0 cellspacing=0>
						<colgroup></colgroup>
						<colgroup></colgroup>
						<colgroup></colgroup>
						<colgroup></colgroup>
						<colgroup></colgroup>
						<thead>
							<tr>
								<th width=150>Дата</th>
								<th width=150>Инициатор</th>
								<th width=150>Объект</th>
								<th width=300>Событие</th>
							</tr>
						</thead>";
			foreach ($res->fetchAll() as $row) {
				// Дата/Инициатор
				echo "		<tr>
								<td width=150 class='date'>".$row['date']."</td>
								<td width=150 class='manager'><span class='manager'>".$manager -> get_fio($row['manager'])."</span></td>";
				// Объект события
				echo "			<td width=150><span class='target_link'>";
				if ( $row['target_type'] == 1 ) {
					echo $contact -> get_fio( $row['target_link'] );
				};
				if ( $row['target_type'] == 2 ) {
					echo $deal -> get_company( $row['target_link'] );
				}
				echo "				</span>
								</td>
								<td width=300 class='event_".$row['event_type']."'><span>";
				
				// Событие
				switch ($row['event_type']) {
					case 1: // Новое примечание
						echo "Новое примечание: ";
					break;
					case 2: // Новая сделка
						echo "Новая сделка: ";
						echo $deal -> get_company($row['event']);
					break;
					case 3: // Новое контакт
						echo "Новый контакт: ";
						echo $contact -> get_fio( $row['event'] );
					break;
					case 4: // Изменение статуса
						echo "Изменение статуса сделки: ";
						echo $deal -> get_company($row['event']);
					break;
					case 5: // Новое письмо
						echo "Новая заметка";
					break;
					case 6: // Удвление контакта
						echo "Удаление контакта";
						echo $contact -> lastInsertId($row['event']);
					break;
				}
				echo"</td></tr>";
				$i++;
			}
			echo "</table>";
		}
	
		public function get_event() {
			if ( ($_SESSION['rang'] == '999') || ($_SESSION['rang'] == '555') ) {
				$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` ORDER BY `date` DESC;");
			} else {
				$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE manager = :manager ORDER BY `date` DESC;");
				$result -> bindValue('manager', $_SESSION['log'], PDO::PARAM_INT);
			}
			$result -> execute();
			$this -> print_event($result);
		}
		
		public function set_event($man, $target_type, $target_link, $event_type, $event) {
			$manager = new _manager();
			$date = date('Y-m-d H:i:s');
			$result = self::$dbh -> prepare("INSERT INTO `".$this->DBName."`.`".$this->table."` (id, date, manager, target_type, target_link, event_type, event) VALUES (null, '$date', :man, :target_type, :target_link, :event_type, :event);");
			$result -> bindValue('man', $manager -> get_id($man), PDO::PARAM_INT);
			$result -> bindValue('target_type', (int)$target_type+0, PDO::PARAM_INT);
			$result -> bindValue('target_link', (int)$target_link+0, PDO::PARAM_INT);
			$result -> bindValue('event_type', (int)$event_type+0, PDO::PARAM_INT);
			$result -> bindValue('event', (int)$event+0, PDO::PARAM_INT);
			if ( $result -> execute() ) { 
				return true ;
			} else { 
				echo $result -> errorInfo; 
				return false; 
			}
		}
		
		public function filter_event($search) {
			$sql = "SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE ".$search.";";
			$result = self::$dbh -> prepare($sql);
			$result -> execute();
			$this -> print_event($result);
		}
	}
?>