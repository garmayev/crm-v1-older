<?	session_start();
	include_once('_db.php');
 	class _task extends _db{

		public $table = 'task';
		
		public function get_task() {
			$deal = new _deal();
			$contact = new _contact();
			$manager = new _manager();
			if ($_SESSION['rang'] != '999') {
				$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE manager = :id ORDER BY `date` DESC;");
				$result -> bindValue('id', $_SESSION['log'], PDO::PARAM_STR);
			} else {
				$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."`;");
			}
			$result -> execute();
			echo "<table border=0 width=100% cellspacing=0 cellpadding=0>
						<thead>
							<tr>
								<td width=30></td>
								<td width=100><strong>Дата</strong></td>
								<td width=100><strong>Время</strong></td>
								<td width=150><strong>Инициатор</strong></td>
								<td width=150><strong>Объект</strong></td>
								<td width=100><strong>Задача</strong></td>
							</tr>
						</thead>";
			foreach ( $result->fetchAll() as $row ) {
				echo "<tr><td width=30><input type='checkbox' id='".$row['id']."'/></td><td width=100 class='date'>";
				echo $row['date'];
				echo "</td><td width=100 class='time'>";
				echo $row['time'];
				echo "</td><td width=150>";
				echo $manager->get_fio( $row['manager'] );
				echo "</td><td width=150>";
				if ( $row['target_type'] == 1 ) {
					echo $contact->get_fio($row['link_type']);
				} else {
					echo $deal->get_company($row['link_type']);
				};
				echo "</td><td width=100 class='type'>";
				switch ($row['type']) {
					case 1:
						echo "Звонок";
					break;
					case 2:
						echo "Письмо";
					break;
					case 3:
						echo "Встреча";
					break;
				};
				echo "</td><td style='display: none'>";
				echo $row['tags'];
				echo "</td><td style='display: none' class='task_status'>";
				echo $row['status'];
				echo "</td><td style='display: none' class='complete'>";
				echo $row['realiz'];
				echo "</td><td style='display: none'>";
				echo $row['tags'];
				echo "</td></tr>";
			}
			echo "</table>";
		}
		
		public function date_task($id) {
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE id = :id");
			$result -> bindValue('id', $id, PDO::PARAM_INT);
			$result -> execute();
			foreach ( $result -> fetchAll() as $row ) {
				return $row['date'];
			}
		}
		
		public function get_status($id) {
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE id = :id");
			$result -> bindValue('id', $id, PDO::PARAM_INT);
			$result -> execute();
			foreach ( $result -> fetchAll() as $row ) {
				return $row['status'];
			}
		}
		
		public function contact($id) {
			$contact = new _contact();
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE target_type = :link AND link_type = :id");
			$result -> bindValue('link', 1, PDO::PARAM_INT);
			$result -> bindValue('id', $id, PDO::PARAM_INT);
			$result -> execute();
			foreach ( $result->fetchAll() as $row ) {
				return $row['id'];
			}
		}
		
		public function target_type($target) {
			$contact = new _contact();
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`contact` WHERE fio = :fio");
			$result -> bindValue('fio', $target, PDO::PARAM_STR);
			$result -> execute();
			if ($result -> fetchAll()) { return 1; } else { return 2; }
		}
		
		public function target_id($target_type, $target) {
			$contact = new _contact();
			$deal = new _deal();
			switch ( $target_type ) {
				case 1:
					$result = self::$dbh -> prepare("SELECT `id` FROM `".$this->DBName."`.`contact` WHERE fio = :fio");
					$result -> bindValue('fio', $target);
					$result -> execute();
					foreach ( $result->fetchAll() as $row ) {
						return $row['id'];
					}
				break;
				case 2:
					$result = self::$dbh -> prepare("SELECT `id` FROM `".$this->DBName."`.`deal` WHERE name = :name");
					$result -> bindValue('name', $target);
					$result -> execute();
					foreach ( $result->fetchAll() as $row ) {
						return $row['id'];
					}
				break;
			}
		}
		
		public function set_task($date, $time, $man, $target, $type, $text) {
			$manager = new _manager();
			$result = self::$dbh -> prepare("INSERT INTO `".$this->DBName."`.`".$this->table."` (id, date, time, manager, target_type, link_type, type, tags, status, realiz) VALUES (null, :date, :time, :manager, :target_type, :link_type, :type, :tags, 0, 0)");
			$result -> bindValue('date', $date, PDO::PARAM_STR);
			$result -> bindValue('time', $time, PDO::PARAM_STR);
			$result -> bindValue('manager', $man, PDO::PARAM_INT);
			$result -> bindValue('target_type', $this->target_type($target), PDO::PARAM_INT);
			$result -> bindValue('link_type', $this->target_id($this->target_type($target), $target), PDO::PARAM_INT);
			$result -> bindValue('type', $type, PDO::PARAM_INT);
			$result -> bindValue('tags', $text, PDO::PARAM_STR);
			$result -> execute();
			var_dump($date, $time, $man, $target, $type, $text, $result);
		}
		
		public function note() {
			$deal = new _deal();
			$contact = new _contact();
			$manager = new _manager();
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."` WHERE manager = :man AND realiz != 1 ORDER BY `date` ASC");
			$result -> bindValue('man', $_SESSION['log'], PDO::PARAM_INT);
			$result -> execute();
			foreach ( $result->fetchAll() as $row ) {
				$date = date("Y-m-d");
				$time = date("H:i");
				if ( $row['date'] > $date ) {
					echo '<div style="border-bottom: 1px solid #ccc" class="more_task" id="'.$row['id'].'">';
					echo '<strong>Дата: '.$row['date'].' '.$row['time'].' - <i>';
					if ( $this -> contact($row['link_type']) == null ) { echo $deal->get_name($row['link_type']); } else { echo $contact->get_fio($row['link_type']); }
					echo '</i></strong><br>'.$row['tags'];
					echo '<br></div>';
				} elseif ( ($row['date'] == $date) && ($row['time'] >= $time) ) {
					echo '<div style="border: 1px solid #ccc;" class="more_task" id="'.$row['id'].'">';
					echo '<strong>Дата: '.$row['date'].' '.$row['time'].' - <i>';
					if ( $this -> contact($row['link_type']) == null ) { echo $deal->get_company($row['link_type']); } else { echo $contact->get_fio($row['link_type']); }
					echo '</i></strong><br>'.$row['tags'];
					echo '<br></div>';
				}
			}
		}
		
		public function task_update($id) {
			$result = self::$dbh -> prepare("UPDATE `".$this->DBName."`.`".$this->table."` SET realiz = :real WHERE id = :id;");
			$result -> bindValue('real', 1, PDO::PARAM_INT);
			$result -> bindValue('id', $id, PDO::PARAM_INT);
			$result -> execute();
		}
		
		public function lastInsertId() { return self::$dbh -> lastInsertId(); }
	}
?>