<?	session_start();

	include('_manager.php');
	include('_contact.php');
	include('_event.php');
	include('_deal.php');
	include('_task.php');
	
	$manager = new _manager();
	$contact = new _contact();
	$event = new _event();
	$deal = new _deal();
	$task = new _task();
	
	function save_deal( $name, $status, $money, $tags, $contact_fio, $company, $state, $phone, $mail, $web, $icq, $address) {
		$deal = new _deal();
		$event = new _event();
		$contact = new _contact();
		$contact -> set_contact ( $contact_fio, $_SESSION['name'], $company, $state, $phone, $mail, $web, $icq, $address, "" );
		$id = $contact -> lastInsertId();
		$event -> set_event( $_SESSION['name'], 1, $id, 3, $id);
		$deal -> set_deal( $company, $name, $status, $money, $_SESSION['name'], $contact_fio, $tags );
		$id = $deal -> lastInsertId();
		$event -> set_event( $_SESSION['name'], 2, $id, 2, $id );
	}

	function add_contact($fio, $manager, $company,  $state, $phone, $mail, $web, $icq, $address, $tags) {
		$event = new _event();
		$contact = new _contact();
		$contact -> set_contact ( $fio, $manager, $company, $state, $phone, $mail, $web, $icq, $address, $tags );
		$id = $contact -> lastInsertId();
		$event -> set_event( $manager, 1, $id, 3, $id);
	}

	function save_task($date, $time, $target, $type, $tags) {
		$task = new _task();
		$event = new _event();
		$contact = new _contact();
		$task -> set_task( $date, $time, $_SESSION['log'], $target, $type, $tags, $status, $realiz );
		$id = $task -> lastInsertId();
		$event -> set_event( $_SESSION['name'], $task->target_type($target), $task->target_id($task->target_type($target), $target), 5, $id );
	}
	
	function deal_manag_update ($id, $state) {
		$deal = new _deal();
		$contact = new _contact();
		$deal -> deal_manag_update($id, $state);
		$contact -> manager_update( $deal -> get_contact( $id ), $state );
	}
	
	function deal_status_update ($id, $state) {
		$deal = new _deal();
		$deal -> deal_status_update($id, $state);
	}
	
	function task_update ($id) {
		$task = new _task();
		echo $task -> task_update ($id);
	}
	
	function deal_del_update($id) {
		$deal = new _deal();
		$event = new _event();
		echo $deal -> deal_del_update($id);
		$event -> set_event( $_SESSION['name'], 2, $id, 4, $id );
	}

	switch ($_POST['func']) {
		case 'event_event':
			echo $event -> get_event();
		break;
		case 'event_deal':
			echo $deal -> get_deal();
		break;
		case 'event_contact':
			echo $contact -> get_contact();
		break;
		case 'event_task':
			echo $task -> get_task();
		break;
		case 'login':
			if ( $manager -> login($_POST['login'], $_POST['pass']) ) { echo "true"; } else { echo "false"; }
		break;
		case "add_deal":
			save_deal( $_POST['name'], $_POST['status'], $_POST['money'], $_POST['tags'], $_POST['contact_fio'], $_POST['org'], $_POST['state'], $_POST['phone'], $_POST['mail'], $_POST['web'], $_POST['icq'], $_POST['address'] );
		break;
		case "save_contact":
			add_contact( $_POST['fio'], $_POST['manager'], $_POST['company'], $_POST['state'], $_POST['phone'], $_POST['mail'], $_POST['web'], $_POST['icq'], $_POST['address'], $_POST['tags'] );
		break;
		case "save_task":
			save_task( $_POST['date'], $_POST['time'], $_POST['target_type'], $_POST['type'], $_POST['tags'] );
		break;
		case "note":
			echo $task -> note();
		break;
		case "live_deal_change":
			deal_status_update($_POST['id'], $_POST['state']);
		break;
		case "live_deal_manag_change":
			deal_manag_update($_POST['id'], $_POST['state']);
		break;
		case "live_deal_del":
			deal_del_update($_POST['id']);
		break;
		case 'task_update':
			task_update($_POST['id']);
		break;
		case "list_manager":
			echo $manager -> list_manager();
		break;
		case "delete_contact":
			$contact -> delete_contact($_POST['id']);
		break;
	}
?>