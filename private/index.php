<? 	session_start();
	if ( $_SESSION['log'] == null ) { echo "<meta http-equiv='refresh' content='0;URL=http://new.ru'>"; exit; };
	include_once('class/_manager.php');
	include_once('class/_contact.php');
	include_once('class/_event.php');
	include_once('class/_deal.php');
	include_once('class/_task.php');
	$manager = new _manager();
	$contact = new _contact();
	$event = new _event();
	$deal = new _deal();
	$task = new _task();
?>
<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		<title>Freeman CRM</title>
		<meta charset="UTF-8" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="js/example.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		<script type="text/javascript" src="js/_contact.js"></script>
		<script type="text/javascript" src="js/_event.js"></script>
		<script type="text/javascript" src="js/_deal.js"></script>
		<script type="text/javascript" src="js/_task.js"></script>
		<script type="text/javascript">
			var prefix = "";
			var i_am = "<? echo $_SESSION['name']; ?>";
			var rang = "<? echo $_SESSION['rang']; ?>";
		</script>
	</head>
	<body>
		<div id='header'>
			<header class="menu">
				<nav align="left">
					<ul style="float: left;">
						<li><a href="#" class="item" id="event">События</a> | </li>
						<li><a href="#" class="item" id="deal">Сделки</a> | </li>
						<li><a href="#" class="item" id="contact">Контакты</a> | </li>
						<li><a href="#" class="item" id="task">Задачи</a></li>
					<ul>
				</nav>
				<div>
					<a href="/"><div class="logo"></div></a>
				</div>
				<nav>
					<ul style="float: right;">
						<? if ( ($_SESSION['rang'] == '999') || ($_SESSION['rang'] == '555') ) {
								echo "<li><a href='#admin' class='item2' id='settings'>Настройки</a></li>"; }
							echo "<li>(<a href='#' class='item2' id='manag'>"; 
							echo $_SESSION['name'];
							echo "</a>)</li>";
						?>
						<li><a href='#' class='item2' id='exit'>Выход</a></li>
					</ul>
				</nav>
			</header>
		</div>
		<section id="wrapper">
			<aside id="contentleft">
				<div id="filter" align="center"><br>
				</div>
			</aside>
			<div id="contentright">
				<div id="table"></div><br>
				<div id="after" style='display: none'></div>
			</div>
		</section>
		<div id="box">
			<div id="open1" class="window">
				<div id="deal_info" style="float: left; width: 250px; padding-right: 10px;">
					<div>
						<strong>Информация о сделке</strong>
					</div>
					<hr>
					<input style="width: 200px;" type="text" id="deal_name" placeholder="Название сделки" /><br>
					<div> Статус </div>
					<div>
						<select style="text-align: left; width: 200px" id="status">
							<option>Первичный контакт</option>
							<option>Переговоры</option>
							<option>Принимают решение</option>
							<option>Согласование договора</option>
							<option>Успешно реализовано</option>
							<option>Закрыто и нереализовано</option>
						</select>
					</div>
					<input style="width: 200px;"  type="text" id="deal_money" placeholder="Бюджет">
					<div>Ответственный</div>
					<div>
					<? 
						if ( $_SESSION['rang'] =='999' ) {
							echo "<select id='deal_manager'>";
							echo $manager -> list_manager();
							echo "</select><br>";
						} else {
							echo "<input id='deal_manager' type='text' placeholder='".$_SESSION['name']."' disabled><br>";
						}
						?>
					</div>
					<textarea style="width: 190px; height: 20px;" id="deal_tags" placeholder='tags'></textarea>
				</div>
				<div id="contact_info" style="float: left; padding-left: 10px; border-left: 1px solid #000;">
					<span><strong>Контакты</strong></span>
					<hr>
					<div>
						<div id='contact'></div>
						<div>
							<input type="text" list="add_contact" id="contact_fio" placeholder="Контакт" /><br>
							<datalist id="add_contact">
								<? echo $contact -> list_contact(); ?>
							</datalist><br>
							<input type="text" id="contact_comp" placeholder="Организация" /><br>
							<input type="text" id="contact_state" placeholder="Должность" />
						</div>
						<div id='phones'>
							<input style="text-align: left; width: 165px; margin-right: 5px;" type="text" id="phone" placeholder="Телефон" />
							<select id="type_phone" style="width: 55px">
								<option>Раб.</option>
								<option>Моб.</option>
								<option>Факс</option>
								<option>Дом</option>
								<option>Дрг.</option>
							</select>
							<a href="#" class="tool" id="add_phone">Добавить еще</a>
						</div>
						<div id='e-mail'>
							<input style="text-align: left; width: 165px; margin-right: 5px;" type="text" list="mail" id="mail" placeholder="E-mail" />
							<select id="manager" style="width: 55px">
								<option>Раб.</option>
								<option>Лич.</option>
								<option>Дрг.</option>
							</select>
							<a href="#" id="add_mail">Добавить еще</a><br>
						</div>
						<div> <input type="text" id="web" placeholder="Сайт" /></div>
						<div> <input type="text" id="icq" placeholder="Jabber"/></div>
						<textarea id="address" placeholder="Адрес"></textarea><br>
					</div>
				<hr>
				<a style="width: 100%;" href="#" class="tool" id="save_deal">Сохранить и выйти</a>&nbsp;<a href="#" class="tool close" id="cancel">Отмена</a>
				</div>
			</div>
			<div id="open2" class="window">
				<div style="width: 49%; float: left;">
				<input type="text" placeholder="Имя Фамилия" style="width: 250px;" id="add_contact_fio"><br>
					<span>Ответственный</span><br>
					<select id="add_contact_manager" style="width: 250px;">
					<? 
						if ( $_SESSION['rang'] =='999' ) {
							echo $manager -> list_manager();
						} else {
							echo "<input type='text' placeholder='".$_SESSION['name']."' disabled><br>";
						}
						?>
					</select>
					<input type="text" style="width: 250px;" placeholder="Название компании" id="add_contact_company" /><br>
					<input type="text" style="width: 250px;" placeholder="Должность" id="add_contact_state" /><br>
					<input type="text" style="width: 250px;" placeholder="Телефон" id="add_contact_phone" /><br>
				</div>
				<div style="width: 49%; flost: left;">
					<input type="text" style="width: 250px;" placeholder="e-mail" id="add_contact_mail" /><br>
					<input type="text" style="width: 250px;" placeholder="Сайт" id="add_contact_web" /><br>
					<input type="text" style="width: 250px;" placeholder="Jabber" id="add_contact_icq" /><br>
					<input type="text" style="width: 250px;" placeholder="Адрес" id="add_contact_address" /><br>
					<textarea style="width: 250px; height: 50px;" placeholder="Теги" id="add_contact_tags" ></textarea><br>
				<a style="width: 100%;" href="#" class="tool" id="save_contact">Сохранить и выйти</a>&nbsp;<a href="#" class="tool close" id="cancel">Отмена</a>
				</div>
			</div>
			<div id="open3" class="window">
				<input type='date' id='date' placeholder='Дата'/>
				<select id='time'>
					<? 	function zeros ($data) {
							$zeros = array('00', '0', '');
							$data = $zeros[strlen((string)$data)]+$data;
							return $data;
						}
						
						for ( $i=0; $i<=47; $i++ ) {
							$hh = zeros( bcdiv(($i * 30), 60) );
							$mm = zeros( ($i * 30) % 60 );
							if ($hh < 10 ) { echo '<option>0'.$hh; } else { echo '<option>'.$hh; };
							if ($mm == 0 ) { echo ':0'.$mm; } else { echo ':'.$mm; };
							echo '</option>';
						}
					?>
				</select>
					<? 
						if ( $_SESSION['rang'] == '999' ) {
							echo "<select id='man'>";
							echo $manager -> list_manager();
							echo "</select><br>";
						} else {
							echo "<input type='text' placeholder='".$_SESSION['name']."' disabled><br>";
						}
					?>
				<select id='target'>
					<? echo $deal -> deal_and_cont() ?>
				</select>
				<a href='#' class='submit' id='1'>Звонок</a> | <a href='#' class='submit' id='2'>Письмо</a> | <a href='#' class='submit' id='3'>Встреча</a><br>
				<textarea style="width: 360px; height: 50px;" id="text"></textarea><br>
				<a style="width: 100%;" href="#" class="tool" id="save_task">Сохранить и выйти</a>&nbsp;<a href="#" class="tool close" id="cancel">Отмена</a>
			</div>
			<div id="mask"></div>
		</div>
		<div class='task'>
			<header class='header_task'>
				<div>
					<span style='float: left'><strong>Текущие задачи</strong></span>
				</div>
				<div>
					<span style='float: right; text-decoration: none;' id='hide'><strong>Hide</strong></span>
					<span style='float: right; text-decoration: none;' id='modal' href='#open3'><strong>Add</strong>&nbsp;|&nbsp;</span>
				</div>
			</header>
			<article class='article_task'>
				<div>
				</div>
			</article>
		</div>
	</body>
</html>