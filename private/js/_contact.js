$(function () {

	$("table tr").live('click', function () {
		if ( rang == '999' ) {
			if ( prefix == 'contact' ) {
				if ( $(this).not("thead tr").hasClass('select') ) { 
					$(this).not("thead tr").removeClass('select'); 
					$(this).not("thead tr").find("input[type=checkbox]").removeAttr('checked');
				} else { 
					$(this).not("thead tr").addClass('select');
					$(this).not("thead tr").find("input[type=checkbox]").attr('checked', 'checked');
				};
				if ( $("tr").not("thead tr").hasClass('select') ) { $('#after').html("<div><a href='#' id='edit'>Изменить статус</a><select></select> | <a href='#' id='manag'>Сменить ответственного</a> | <a href='#' id='delete'>Удалить</a></div>").show("fast"); } else { $('#after').hide('fast') }
			};
		}
	});

	function insert_ajax(data) {
		$.ajax({
			url: "class/script.php",
			type: "post",
			data: data,
			success: function (html) {
				alert(html);
			}
		});
	}

	function filter () {
		$("table").animate( {opacity:0}, 200, "linear", function(){
			var today = new Date();
			var YY = today.getFullYear();
			var MM = today.getMonth();
			var DD = today.getDate();
			today = YY+"-"+MM+"-"+DD;
			if ( prefix == 'contact' ) {
				$("table tr").show("fast");
				id = $("div.submit_down").attr('id');
				switch (id) {
					case "contact_all":
						;
					break;
					case "contact_until":
						$("table tr").not("thead tr").each(function () {
							if ( $(this).find("td.task_id").text() == '' ) { $(this).css({"display": "none"}) };
						})
					break;
					case "contact_without":
						$("table tr").not("thead tr").each(function () {
							if ( ($(this).find("td.task_status").text() != '') && ($(this).find("td.task_date").text() < today) ) {
								$(this).css({"display": "none"});
							};
						})
					break;
				}
				
				$("table tr").not("thead tr").each( function() {
					for ( var i=0; i<=6; i++) {
						if ( $(this).find("td.deal_status").text() == $("input."+i).next().text() ) {
							if ( $("input."+i).prop('checked') ) {
								$(this).css("display", "inline inline");
							} else {
								$(this).css("display", "none")
							}
						}
					}
				})
			}
		}).animate( {opacity:1}, 200, "linear" );
	}
	
	$("div.submit").live('click', function () {
		if ( prefix == 'contact' ) {
			$("div.submit").each( function () { $(this).removeClass("submit_down") } )
			$(this).addClass("submit_down");
			filter()
		}
	})
	
	$("input").live('change', function () { 
		if ( prefix == 'contact' ) { 
			filter(); 
		} 
	})

	$("span.checkbox").live('click', function () {
		if ( $(this).prev().prop('checked') ) { 
			$(this).prev().removeAttr('checked') 
		} else { 
			$(this).prev().attr('checked', 'checked');
		}
		if ( prefix == 'contact' ) {
			filter();
		}
	});
	
	$("input#modal").live('click', function () {
		if ( prefix == 'contact' ) {
			var id = $(this).attr('href');
			var maskH = $(document).height();
			var maskW = $(window).width();
			$('#mask').css({'width': maskW, 'height': maskH, 'display': 'block', 'background': '#aaa'});
			$('#mask').fadeIn(700);
			$('#mask').fadeTo('fast', 0.9);
			var winH = $(window).height();
			var winW = $(window).width();
			$(id).css({'top': winH/2 - $(id).height()/2});
			$(id).css({'left': winW/2 - $(id).width()/2});
			$(id).fadeIn(700);
			return false;
		}
	})
	
	$("#mask, a.close").live('click', function () {
		if ( prefix == 'contact' ) {
			$("#mask").hide('fast');
			$('.window').hide('fast');
		}
	})

	$("a#save_contact").live('click', function () {
		var data = "func=save_contact&fio="+$("#add_contact_fio").val()+"&manager="+$("#add_contact_manager").val()+"&company="+$("#add_contact_company").val()+"&state="+$("#add_contact_state").val()+"&phone="+$("#add_contact_phone").val()+"&mail="+$("#add_contact_mail").val()+"&web="+$("#add_contact_web").val()+"&icq="+$("#add_contact_icq").val()+"&address="+$("#add_contact_address").val()+"&tags="+$("#add_contact_tags").val();
		insert_ajax(data);
		$('#mask, .window').hide('fast');
	})

	$("a#manag").live('click', function () {
		if (prefix == 'contact') {
			$("#live_status").hide("fast");
			list_manager();
		}
	})
	
	function list_manager() {
		$.ajax({
			url: 'class/script.php',
			type: 'post',
			data: 'func=list_manager',
			success: function (html) {
				$('select#live_man').html(html).show("fast");
			}
		})
	}
	
	$("#live_status").live('change', function () {
		$('table tr').find('input:checked').each( function () {
			var data = "func=live_deal_change&id="+$(this).attr('id')+"&state="+$('#live_status').val();
			$.ajax({
				url: 'class/script.php',
				type: 'post',
				data: data,
				success: function (html) {
					$('a#event').trigger('click');
				}
			})
		})
	})
	
	$('#live_man').live('change', function () {
		$('table tr').find('input:checked').each( function () {
			var data = "func=live_deal_manag_change&id="+$(this).attr('id')+"&state="+$('#live_man').val();
			$.ajax({
				url: 'class/script.php',
				type: 'post',
				data: data,
				success: function (html) {
					$('a#event').trigger('click');
				}
			})
		})
	})

	$("a#delete").live('click', function () {
		$('table tr').each( function () {
			if ( $(this).find('input[type=checkbox]').prop('checked') ) {
				$.ajax({
					url: 'class/script.php',
					type: 'post',
					data: data = 'func=delete_contact&id='+$(this).attr('id')
				});
			}
		})
	});
})