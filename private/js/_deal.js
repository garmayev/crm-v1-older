$(function () {

	function select_hide() {
		$("#after a").next("select").hide("fast");
	}

	function reset_deal() {
		$('#deal_name').val('');
		$('#status').val('Первичный контакт');
		$('#deal_money').val('');
		$('#deal_tags').val('');
		$('div#contact').html('');
		$('#contact_fio').val('');
		$('#contact_comp').val('');
		$('#contact_state').val('');
		$('#phone').val('');
		$('div#phones').html('<input style="text-align: left; width: 165px; margin-right: 5px;" type="text" id="phone" placeholder="Телефон" /><select id="type_phone" style="width: 55px"><option>Раб.</option><option>Моб.</option><option>Факс</option><option>Дом</option><option>Дрг.</option></select><a href="#" class="tool" id="add_phone">Добавить еще</a>');
		$('div#e-mail').html('<input style="text-align: left; width: 165px; margin-right: 5px;" type="text" list="mail" id="mail" placeholder="E-mail" /><select id="manager" style="width: 55px"><option>Раб.</option><option>Лич.</option><option>Дрг.</option></select><a href="#" id="add_mail">Добавить еще</a><br>');
		$('#web').val('');
		$('#icq').val('');
		$('#address').val('');
	}

	function list_manager() {
		$.ajax({
			url: 'class/script.php',
			type: 'post',
			data: 'func=list_manager',
			success: function (html) {
				$('select#live_man').html('<option>Выбарите менеджера</option>'+html).show("fast");
			}
		})
	}

	function inset_ajax(data) {
		$.ajax({
			url: "class/script.php",
			type: "post",
			data: data,
			success: function (html) {
				alert(html);
			}
		});
	}

	function filter() {
		$("table").animate( {opacity:0}, 200, "linear", function(){
			if ( prefix == "deal" ) {
				$("table tr").show("fast");
				var date1, date2;
				if ( $("#deal_start_date").val() != '' ) { date1 = $("#deal_start_date").val()+" 00:00:00"; };
				if ( $("#deal_finish_date").val() != '') { date2 = $("#deal_finish_date").val()+" 23:59:59"; };
				$("table tr").not("thead tr").each( function () {
					if ( (date1 != '') && (date2 != '') ) {
						if ( ($(this).find("td.date").text() < date1) && ($(this).find("td.date").text() > date2) ) { $(this).css({"display": "none"}) };
					}
				})
				id = $("div.submit_down").attr('id');
				switch (id) {
					case "deal_open":
						$("table tr").not("thead tr").each(function () {
							if ( $(this).find("td").hasClass('status_5') ) { $(this).css({"display": "none"}); };
							if ( $(this).find("td").hasClass('status_6') ) { $(this).css({"display": "none"}); };
						})
					break;
					case "deal_my":
						$("table tr").not("thead tr").each(function () {
							if ( $(this).find("td.manager").text() != i_am ) { $(this).css({"display": "none"}) };
						})
					break;
					case "deal_good":
						$("table tr").not("thead tr").each(function () {
							if ( !$(this).find("td").hasClass('status_5') ) { $(this).css({"display": "none"}); };
						})
					break;
					case "deal_fail":
						$("table tr").not("thead tr").each(function () {
							if ( !$(this).find("td").hasClass('status_6') ) { $(this).css({"display": "none"}); };
						})
					break;
				}
				for ( var i=1; i<=5; i++ ) {
					$("tr:visible td span").not("thead tr").each( function() {
						if ( $(this).attr('class') == "status_"+i ) {
							if ( !$("input#event_"+i).prop("checked") ) { $(this).parent().parent().css({"display": "none"}) } else { $(this).parent().parent().show() };
						}
					})
				};
			}
		}).animate( {opacity:1}, 200, "linear" );
	}

	$("table tr").live('click', function () {
		if ( prefix == 'deal' ) {
			if ( rang == '999' ) {
				if ( $(this).not("thead tr").hasClass('select') ) { 
					$(this).not("thead tr").removeClass('select'); 
					$(this).not("thead tr").find("input[type=checkbox]").removeAttr('checked');
				} else { 
					$(this).not("thead tr").addClass('select');
					$(this).not("thead tr").find("input[type=checkbox]").attr('checked', 'checked');
				};
				if ( $("tr").not("thead tr").hasClass('select') ) { $('#after').html("<div><a href='#' id='edit'>Изменить статус</a><select id='live_status' style='display: none'></select> | <a href='#' id='manager'>Сменить ответственного</a><select id='live_man' style='display: none'></select> | <a href='#' id='delete'>Удалить</a>").show("fast"); } else { $('#after').hide('fast') }
			};
		};
	});

	$('a#delete').live('click', function () {
		$('tbody tr').each( function () {
			if ( $(this).find('input[type=checkbox]').prop('checked') ) {
				alert( $(this).attr('id') );
			}
		});
	})
	
	$("span.deal_checked_box").live('click', function () {
		if ( $(this).prev().prop('checked') ) { 
			$(this).prev().removeAttr('checked');
		} else { 
			$(this).prev().attr('checked', 'checked');
		}
		if ( prefix == 'deal' ) {
			filter();
		}
	});
	
	$(".submit").live('click', function () {
		if ( prefix == 'deal' ) {
			$(".submit").removeClass('submit_down');
			$(this).addClass('submit_down');
			filter();
		}
	});
	
	$(":checkbox").live("click", function () {
		if ( prefix == 'deal' ) { filter(); };
	})
	
	$("input#modal").live('click', function () {
		if ( prefix == 'deal' ) {
			var id = $(this).attr('href');
			var maskH = $(document).height();
			var maskW = $(window).width();
			$('#mask').css({'width': maskW, 'height': maskH, 'display': 'block', 'background': '#aaa'});
			$('#mask').fadeIn(700);
			$('#mask').fadeTo('fast', 0.9);
			var winH = $(window).height();
			var winW = $(window).width();
			$(id).css({'top': winH/2 - $(id).height()/2});
			$(id).css({'left': winW/2 - $(id).width()/2});
			$(id).fadeIn(700);
			return false;
		}
	})
	
	$("div#mask, .close").live('click', function () {
		if ( prefix == 'deal' ) {
			reset_deal();
			$('#mask').hide('fast');
			$('.window').hide('fast');
		}
	})
		
	$("a#save_deal").live("click", function () {
		var data = "func=add_deal&name="+$("input#deal_name").val()+"&status="+$("select#status").val()+"&money="+$("input#deal_money").val()+"&manag="+$("select#deal_manager").val()+"&tags="+$("textarea#deal_tags").val();
		// alert( $('#contact').html() );
		// if ( $('#contact').html() ) {
			data += "&contact_fio="+$("input#contact_fio").val()+"&org="+$("#contact_comp").val()+"&state="+$("input#contact_state").val()+"&phone="+$("input#phone").val()+"&mail="+$("input#mail").val()+"&web="+$("input#web").val()+"&icq="+$("#icq").val()+"&address="+$("textarea#address").val();
		// }
		inset_ajax(data);
		$("div#mask").trigger("click");
		$('a#event').trigger('click');
	})

	$("input#contact_fio").live('blur', function () {
		text = $(this).val();
		$('datalist#add_contact option').each( function () {
			if ( text == $(this).attr('value') ) {
				if ( $('div#contact').text().search(text) < 0 ) {
					$('div#contact').append(text+'<br>');
					$('input#contact_fio').focus();
					$('input#contact_fio').val('');
				}
			};
		});
	})

	$("a#manager").live('click', function () {
		if (prefix == 'deal') {
			$("#live_status").hide("fast");
			list_manager();
		}
	})

	$("#live_status").live('change', function () {
		$('table tr').find('input:checked').each( function () {
			var data = "func=live_deal_change&id="+$(this).attr('id')+"&state="+$('#live_status').val();
			$.ajax({
				url: 'class/script.php',
				type: 'post',
				data: data,
				success: function (html) {
					$('a#event').trigger('click');
				}
			})
		})
	})

	$('#live_man').live('change', function () {
		$('table tr').find('input:checked').each( function () {
			var data = "func=live_deal_manag_change&id="+$(this).attr('id')+"&state="+$('#live_man').val();
			$.ajax({
				url: 'class/script.php',
				type: 'post',
				data: data,
				success: function (html) {
					$('a#event').trigger('click');
				}
			})
		})
	})

	$("a#edit").live("click", function () {
		$("#live_man").hide("fast");
		if (prefix == 'deal') {
			$("#live_status").html("<option>Выберите статус</option>"+
								"<option>Первичный контакт</option>"+
								"<option>Переговоры</option>"+
								"<option>Принимают решение</option>"+
								"<option>Согласование договора</option>"+
								"<option>Успешно реализовано</option>"+
								"<option>Закрыто и не реализовано</option>").show("fast");
		}
	});	
})