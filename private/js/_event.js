$(function () {

		var date = new Date();
		var today = new Date();
		var YY = today.getFullYear();
		var MM = today.getMonth();
		var DD = today.getDate();
		var hh = today.getHours();
		var mm = today.getMinutes();
		var ss = today.getSeconds();

		function DayBefore(day) {
			newDD = DD - day;	// Делаем 
			if ( newDD < 1 ) {
				newMM = MM - 1;
				if ( newMM < 1 ) {
					YY = YY - 1;
				} else {
					if ( ( newMM % 2 == 0 ) && ( newMM % 7 != 1 ) ) {
						if  (newMM == 2) {
							if ( ( YY % 100 == 0 ) && ( YY % 400 == 0 ) ) { newDD = 29 + newDD; } else { newDD = 28 + newDD; }
						} else {
							if ( newMM % 2 == 0 ) { newDD = 30 + newDD; }
						}
					} else {
						if ( ( newMM % 2 == 1 ) && ( newMM % 7 != 1 ) ) { newDD = 31 + newDD; }
					};
					if ( ( newMM % 2 == 0 ) && ( newMM % 7 == 1 ) ) { 
						newDD = 31 + newDD;
					} else {
						newDD = 30 + newDD;
					}
				}
				MM = newMM;
			};
			DD = newDD;
		}
		
		function zeros (date) {
			zero = ["00", "0", ""];
			date = zero[date.length]+date;
			return date;
		}
		
		function filter() {
			$("table").animate( {opacity:0}, 200, "linear", function(){
				if ( prefix == 'event' ) {
					$("table tr").show("fast");
					if ( !$("#select_period").prop('checked') ) {
						switch ( $("select#event_select_period").val() ) {
							case "event_all_time":
								YY = YY - 5;
							break;
							case "event_today":
								DayBefore(1);
							break;
							case "event_three":
								DayBefore(3);
							break;
							case "event_week":
								DayBefore(7);
							break;
							case "event_month":
								MM = MM - 1;
								if (MM < 0) { 
									MM = 12; 
									YY = YY - 1; 
								}
							break;
							case "event_semi":
								newMM = MM - 6;
								if (newMM < 0) { 
									MM = 12 + newMM; 
									YY = YY - 1; 
								} else { 
									MM = newMM 
								}
							break;
						}
						MM = MM + 1;
						query = YY+"-";
						if ( MM < 9 ) { 
							query = query+'0'+MM+"-"; 
						} else {
							query = query+MM+"-"; 			
						};
						if ( DD < 9 ) { 
							query += '0'+DD+" 23:59:59"; 
						} else {
							query += DD.toString()+" 23:59:59"; 
						};
						$("table tr").not("thead tr").each( function () {
							if ( $(this).find(".date").text() < query ) { $(this).css({"display": "none"}) };
						});
						YY = today.getFullYear();
						MM = today.getMonth();
						DD = today.getDate();
						hh = today.getHours();
						mm = today.getMinutes();
						ss = today.getSeconds();
					} else {
						date1 = $("#event_start_date").val();
						date2 = $("#event_finish_date").val();
						$("table tr").not("thead tr").each( function () {
							if ( ($(this).find("td.date").text() < date1) && ($(this).text() > date2) ) { $(this).css({"display": "none"}) } });
					}
					for ( var i=1; i<=5; i++ ) {
						$("tr td").not("thead  tr").each( function() {
							if ( $(this).attr('class') == "event_"+i ) {
								if ( !$("input#event_"+i).prop("checked") ) { $(this).parent().css({"display": "none"}) }
							}
						})
					};
				};
			}).animate( {opacity:1}, 200, "linear" );
		}
		
		$("a#event_search_period").live('click', function () {
			if ( prefix =="event" ) {
				if ( $(this).hasClass('event_filter') ) {
					filter();
				}
			}
		})
		
		$("input[type=checkbox].filter_event").live('change', function () {
			if ( prefix == 'event' ) { filter(); };
		})
		
		$("span.event_filter").live('click', function () {
			if (prefix == 'event') {
				if ( $(this).prev().prop('checked') ) { 
					$(this).prev().removeAttr('checked'); 
				} else { 
					$(this).prev().attr('checked', 'checked'); 
				};
				filter();
			}
		})
		
		$("#select_period").live('change', function () {
			if ( $(this).prop('checked') ) {
				$('#event_start_date').removeAttr('disabled');
				$('#event_finish_date').removeAttr('disabled');
				$('#event_select_period').attr('disabled', 'disabled');
			} else {
				$('#event_start_date').attr('disabled', 'disabled');
				$('#event_finish_date').attr('disabled', 'disabled');
				$('#event_select_period').removeAttr('disabled');
			}
		})

		$("select#event_select_period").live("change", function () {
			filter();
			zebra();
		})
		
})