$(function () {

	$("table tr").live('click', function () {
		if ( prefix == 'task' ) {
			if ( $(this).not("thead tr").hasClass('select') ) { 
				$(this).not("thead tr").removeClass('select'); 
				$(this).not("thead tr").find("input[type=checkbox]").removeAttr('checked');
			} else { 
				$(this).not("thead tr").addClass('select');
				$(this).not("thead tr").find("input[type=checkbox]").attr('checked', 'checked');
			};
			if ( $("tr").not("thead tr").hasClass('select') ) { $('#after').html("<div><a href='#' id='delete'>Завершить</a></div>").show("fast"); } else { $('#after').hide('fast') }
		};
	});

	function insert_ajax(data) {
		$.ajax({
			url: "class/script.php",
			type: "post",
			data: data,
			success: function (html) {
				$("a#event").trigger("click");
			}
		});
	}
	
	function filter () {
		if ( prefix == 'task' ) {
			$("table").animate( {opacity:0}, 200, "linear", function(){
				$("table tr").not("thead").show();
				var today = new Date();
				var YY = today.getFullYear();
				var MM = today.getMonth();
				var DD = today.getDate();
				var hh = today.getHours();
				var mm = today.getMinutes();
				today = YY+"-"+(MM + 1)+"-"+DD;
				tohour = hh+":"+mm
				
				switch ( $('div.submit_down').attr('id') ) {
					case 'my_task':
						$('table tr').not('thead tr').each( function () {
							if ( $(this).text().search(i_am) > 0 ) { $(this).show() } else { $(this).css("display", "none"); }
						})
					break;
					case 'my_none_task':
						$('table tr').not('thead tr').each( function () {
							if ( ($(this).text().search(i_am) > 0) && ($(this).find('td.date').text() <= today) && ($(this).find('td.time').text() <= tohour ) && ($(this).find('td.complete').text().search('1') != 0  )) { 
								$(this).show();
							} else { $(this).hide(); }
						})
					break;
					case "my_finish_task":
						$('table tr').not('thead tr').each( function () {
							if ( ($(this).text().search(i_am) > 0) && ($(this).find('td.complete').text().search('1') == 0 ) ) { 
								$(this).show(); 
							} else { 
								$(this).css("display", "none"); 
							}
						})
					break;
					case "all_task":
						$('table tr').not('thead tr').show();
					break;
				}
				$('input.task_type').each( function () {
					id = $(this).attr('id');
					switch ( $(this).attr('id') ) {
						case "Звонок":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.type').text().search(id) == 0 ) { $(this).hide(); }
								})
							}
						break;
						case "Встреча":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.type').text().search(id) == 0 ) { $(this).hide(); }
								})
							}
						break;
						case "Письмо":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.type').text().search(id) == 0 ) { $(this).hide(); }
								})
							}
						break;
					}
				})
				$('input[type=checkbox].task_status').each( function () {
					switch ( $(this).attr('id') ) {
						case "none":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.complete').text().search('0') == 0 ) { $(this).css('display', 'none'); }
								})
							}
						break;
						case "good":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.complete').text().search('0') != 0 ) { $(this).css('display', 'none'); }
								})
							}
						break;
					}
				})
			}).animate( {opacity:1}, 200, "linear" );
		}
	}

	$("input#modal").live('click', function () {
		if ( prefix == 'task' ) {
			var id=$(this).attr('href');
			var maskH = $(document).height();
			var maskW = $(window).width();
			$('#mask').css({'width': maskW, 'height': maskH, 'display': 'block', 'background': '#aaa'});
			$('#mask').fadeIn(700);
			$('#mask').fadeTo('fast', 0.9);
			var winH = $(window).height();
			var winW = $(window).width();
			$(id).css({'top': winH/2 - $(id).height()/2});
			$(id).css({'left': winW/2 - $(id).width()/2});
			$(id).fadeIn(700);
			return false;
		}
	})
	
	$("#mask, a.close").live('click', function () {
		if ( prefix == 'task' ) {
			$(this).hide('fast');
			$('.window').hide('fast');
			return false;
		}
	})
	
	$('.submit').live('click', function () {
		if ( prefix == 'task' ) { 
			$('.submit').each(function () { $(this).removeClass('submit_down') })
			$(this).addClass('submit_down'); 
			filter();
		}
	})
	
	$('input[type=checkbox]').live('click', function () {
		filter();
	})

	$('a#save_task').live('click', function () {
		var data = "func=save_task&date="+$("#date").val()+"&time="+$("#time").val()+"&manager="+$("#man").val()+"&target_type="+$('#target').val()+"&type="+$('a.submit_down').attr('id')+"&tags="+$('textarea#text').val();
		insert_ajax(data);
		$('#mask, a.close').trigger('click');
		$('a#event').trigger('click');
	})
	
	function realiz_task(id) {
		alert(id);
	}
	
	$("a#delete").live("click", function () {
		$("table tr").find("td input[type=checkbox]:checked").each( function () {
			$.ajax({
				url: 'class/script.php',
				type: 'post',
				data: 'func=task_update&id='+$(this).attr('id'),
				success: function (html) {
					// alert(html);
					$("a#event").trigger('click');
				}
			})
		})
	})
})