$(function () {

	var selected = false;
	var task_flag = false;
	
	prefix = "event";
	
	$("#filter").animate( {opacity:0}, 200, "linear", function(){ $(this).html(
		"<div style='display: block-inline;'>"+
			"Период: "+
			"<select style='width: 170px;' class='event_filter' id='event_select_period'>"+
				"<option value='event_all_time'>За все время</option>"+
				"<option value='event_today'>За сегодня</option>"+
				"<option value='event_three'>За три дня</option>"+
				"<option value='event_week'>За неделю</option>"+
				"<option value='event_month'>За месяц</option>"+
				"<option value='event_semi'>За полгода</option>"+
			"</select><br>"+
			"<input class='event_filter' type='checkbox' id='select_period' /><span>Выбрать период</span><br>"+
			"<input disabled class='event_filter' id='event_start_date' style='width: 100px' type='text' placeholder='Дата начала' /> - "+
			"<input disabled class='event_filter' id='event_finish_date' style='width: 100px' type='text' placeholder='Дата конца' /><br>"+
			"<a href='#' id='event_search_period'>Отфильтровать по дате</a><br><br>"+
		"</div>"+
		"<div style='display: block-inline;'>"+
			"<input type='checkbox' checked class='filter_event' id='event_1' /><span class='event_filter'>Примечание</span><br>"+
			"<input type='checkbox' checked class='filter_event' id='event_2' /><span class='event_filter'>Сделка</span><br>"+
			"<input type='checkbox' checked class='filter_event' id='event_3' /><span class='event_filter'>Контакт</span><br>"+
			"<input type='checkbox' checked class='filter_event' id='event_4' /><span class='event_filter'>Изменение статуса</span><br>"+
			"<input type='checkbox' checked class='filter_event' id='event_5' /><span class='event_filter'>Новая задача</span><br>"+
		"</div>").animate( {opacity:1}, 200, "linear" ); } );
	
	set_ajax(prefix);
	
	function filter() {
		if ( prefix == 'deal' ) {
			$("table").animate( {opacity:0}, 200, "linear", function(){
				if ( prefix == "deal" ) {
					$("table tr").show("fast");
					var date1, date2;
					if ( $("#deal_start_date").val() != '' ) { date1 = $("#deal_start_date").val()+" 00:00:00"; };
					if ( $("#deal_finish_date").val() != '') { date2 = $("#deal_finish_date").val()+" 23:59:59"; };
					$("table tr").not("thead tr").each( function () {
						if ( (date1 != '') && (date2 != '') ) {
							if ( ($(this).find("td.date").text() < date1) && ($(this).find("td.date").text() > date2) ) { $(this).css({"display": "none"}) };
						}
					})
					id = $("div.submit_down").attr('id');
					switch (id) {
						case "deal_open":
							$("table tr").not("thead tr").each(function () {
								if ( $(this).find("td").hasClass('status_5') ) { $(this).css({"display": "none"}); };
								if ( $(this).find("td").hasClass('status_6') ) { $(this).css({"display": "none"}); };
							})
						break;
						case "deal_my":
							$("table tr").not("thead tr").each(function () {
								if ( $(this).find("td.manager").text() != i_am ) { $(this).css({"display": "none"}) };
							})
						break;
						case "deal_good":
							$("table tr").not("thead tr").each(function () {
								if ( !$(this).find("td").hasClass('status_5') ) { $(this).css({"display": "none"}); };
							})
						break;
						case "deal_fail":
							$("table tr").not("thead tr").each(function () {
								if ( !$(this).find("td").hasClass('status_6') ) { $(this).css({"display": "none"}); };
							})
						break;
					}
					for ( var i=1; i<=5; i++ ) {
						$("tr:visible td span").not("thead tr").each( function() {
							if ( $(this).attr('class') == "status_"+i ) {
								if ( !$("input#event_"+i).prop("checked") ) { $(this).parent().parent().css({"display": "none"}) } else { $(this).parent().parent().show() };
							}
						})
					};
				}
			}).animate( {opacity:1}, 200, "linear" );
		}
		if ( prefix == 'contact' ) {
			$("table").animate( {opacity:0}, 200, "linear", function(){
				var today = new Date();
				var YY = today.getFullYear();
				var MM = today.getMonth();
				var DD = today.getDate();
				today = YY+"-"+MM+"-"+DD;
				if ( prefix == 'contact' ) {
					$("table tr").show("fast");
					id = $("div.submit_down").attr('id');
					switch (id) {
						case "contact_all":
							;
						break;
						case "contact_until":
							$("table tr").not("thead tr").each(function () {
								if ( $(this).find("td.task_id").text() == '' ) { $(this).css({"display": "none"}) };
							})
						break;
						case "contact_without":
							$("table tr").not("thead tr").each(function () {
								if ( ($(this).find("td.task_status").text() != '') && ($(this).find("td.task_date").text() < today) ) {
									$(this).css({"display": "none"});
								};
							})
						break;
					}
				
					$("table tr").not("thead tr").each( function() {
						for ( var i=1; i<=6; i++) {
							if ( $(this).find("td.deal_status").text() == $("input."+i).attr("class") ) {
								if ( $("input."+i).prop('checked') ) {
									$(this).css("display", "inline inline");
								} else {
									$(this).css("display", "none")
								}
							}
						}
						if ( $(this).find("td.deal_status").text() == '') {
							if ($("input.0").prop('checked')) {
								$(this).css("display", "inline inline");
							} else {
								$(this).css("display", "none");
							}
						}
					})
					$("table tr:visible").not("thead tr").each( function() {
						for ( var i=0; i<=6; i++) {
							if ( $(this).find("td.deal_status").text() == $("input."+i).next().text() ) {
								if ( $("input."+i).prop('checked') ) {
									$(this).css("display", "inline inline");
								} else {
									$(this).css("display", "none")
								}
							}
						}
					})
				}
			}).animate( {opacity:1}, 200, "linear" );
		}
		if ( prefix == 'task' ) {
			$("table").animate( {opacity:0}, 200, "linear", function(){
				$("table tr").not("thead").show();
				var today = new Date();
				var YY = today.getFullYear();
				var MM = today.getMonth();
				var DD = today.getDate();
				var hh = today.getHours();
				var mm = today.getMinutes();
				today = YY+"-"+(MM + 1)+"-"+DD;
				tohour = hh+":"+mm
				
				switch ( $('div.submit_down').attr('id') ) {
					case 'my_task':
						$('table tr').not('thead tr').each( function () {
							if ( $(this).text().search(i_am) > 0 ) { $(this).show() } else { $(this).css("display", "none"); }
						})
					break;
					case 'my_none_task':
						$('table tr').not('thead tr').each( function () {
							if ( ($(this).text().search(i_am) > 0) && ($(this).find('td.date').text() <= today) && ($(this).find('td.time').text() <= tohour ) && ($(this).find('td.complete').text().search('1') != 0  )) { 
								$(this).show();
							} else { $(this).hide(); }
						})
					break;
					case "my_finish_task":
						$('table tr').not('thead tr').each( function () {
							if ( ($(this).text().search(i_am) > 0) && ($(this).find('td.complete').text().search('1') == 0 ) ) { 
								$(this).show(); 
							} else { 
								$(this).css("display", "none"); 
							}
						})
					break;
					case "all_task":
						$('table tr').not('thead tr').show();
					break;
				}
				$('input.task_type').each( function () {
					id = $(this).attr('id');
					switch ( $(this).attr('id') ) {
						case "Звонок":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.type').text().search(id) == 0 ) { $(this).hide(); }
								})
							}
						break;
						case "Встреча":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.type').text().search(id) == 0 ) { $(this).hide(); }
								})
							}
						break;
						case "Письмо":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.type').text().search(id) == 0 ) { $(this).hide(); }
								})
							}
						break;
					}
				})
				$('input[type=checkbox].task_status').each( function () {
					switch ( $(this).attr('id') ) {
						case "none":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.complete').text().search('0') == 0 ) { $(this).css(	'display', 'none'); }
								})
							}
						break;
						case "good":
							if ( !$(this).prop("checked") ) {
								$('table tr').not('thead tr').each( function () {
									if ( $(this).find('td.complete').text().search('0') != 0 ) { 
										$(this).css('display', 'none'); 
									}
								})
							}
						break;
					}
				})
			}).animate( {opacity:1}, 200, "linear" );
		}
	}

	function search (query) {
		if ( query != '' ) {
			query = $.trim(query);
			query = query.replace(/ /gi, '|');
			$("table tr").not('thead tr').each( function () {
				( $(this).text().search(new RegExp(query, "i")) < 0 ) ? $(this).hide() : $(this).show();
			})
		} else { $("table").show() }
	}
	
	function getChar(event) {
		if (event.which == null) {
			if (event.keyCode < 32) return null;
			return String.fromCharCode(event.keyCode)
		}
		if (event.which!=0 && event.charCode!=0) {
			if (event.which < 32) return null;
			return String.fromCharCode(event.which);
		}
		return null;
	}
	
	function set_ajax(prefix) {
		$.ajax({
			url: "class/script.php",
			type: "post",
			data: "func=event_"+prefix,
			success: function (html) {
				$("#table").animate( {opacity:0}, 200, "linear", function(){ $(this).html(html).animate( {opacity:1}, 200, "linear" ); } );
				filter();
			}
		})
	}
	
	$("table tr").live("mouseenter", function () {
		$(this).not("thead tr").addClass("enter");
	});
	
	$("table tr").live("mouseleave", function () {
		$(this).not("thead tr").removeClass("enter");
	});
	
	$("a").live("click", function () {
		if ( $(this).attr('href') != 'http://www.fmst.org/' ) { return false; }
	})
	
	$("a#event").on("click", function () {
		if ( prefix != "event" ) {
			prefix = "event";
			set_ajax(prefix);
			$("#filter").animate( {opacity:0}, 200, "linear", function(){ $(this).html(
				"<div>"+
					"Период: "+
					"<select style='width: 170px' class='event_filter' id='event_select_period'>"+
						"<option value='event_all_time'>За все время</option>"+
						"<option value='event_today'>За сегодня</option>"+
						"<option value='event_three'>За три дня</option>"+
						"<option value='event_week'>За неделю</option>"+
						"<option value='event_month'>За месяц</option>"+
						"<option value='event_semi'>За полгода</option>"+
					"</select><br>"+
					"<input class='event_filter' type='checkbox' id='select_period' /><span>Выбрать период</span><br>"+
					"<input disabled class='event_filter' id='event_start_date' style='width: 75px' type='text' placeholder='Дата начала' /> - "+
					"<input disabled class='event_filter' id='event_finish_date' style='width: 75px' type='text' placeholder='Дата конца' /><br>"+
					"<a href='#' id='event_search_period'>Отфильтровать по дате</a>"+
				"</div>"+
				"<div style='display: block-inline;'>"+
					"<input type='checkbox' checked class='filter_event' id='event_1' /><span class='event_filter'>Примечание</span><br>"+
					"<input type='checkbox' checked class='filter_event' id='event_2' /><span class='event_filter'>Сделка</span><br>"+
					"<input type='checkbox' checked class='filter_event' id='event_3' /><span class='event_filter'>Контакт</span><br>"+
					"<input type='checkbox' checked class='filter_event' id='event_4' /><span class='event_filter'>Изменение статуса</span><br>"+
					"<input type='checkbox' checked class='filter_event' id='event_5' /><span class='event_filter'>Новое письмо</span><br>"+
				"</div>").animate( {opacity:1}, 200, "linear" ); } );
			$('#after').hide("fast");
		}
		return false;
	});
	
	$("a#deal").on("click", function () {
		if ( prefix != "deal" ) {
			prefix = "deal";
			set_ajax(prefix);
			$("#filter").animate( {opacity:0}, 500, "linear", function(){ $(this).html(
				"<div style='float: left;'>"+
					"<input type='button' href='#open1' id='modal' id='add_deal' class='deal_filter' style='width: 240px; height: 30px;' value='Добавить сделку' /><br><br>"+
					"<input type='text' class='deal_filter' id='search' style='width: 230px;' placeholder='Поиск сделки' /><br>"+
					"<div align='center'><strong>Период:&nbsp;</strong>"+
					"<input type='text' id='deal_start_date' style='width: 70px;' /> - "+
					"<input type='text' id='deal_finish_date' style='width: 70px;' /></div><br>"+
				"</div>"+
				"<div style='float: left;'>"+
					"<div class='deal_filter' align='center'><div class='submit' id='deal_open'>Открытые сделки&nbsp;</div></div>"+
					"<div class='deal_filter' align='center'><div class='submit' id='deal_my'>Только мои сделки&nbsp;</div></div>"+
					"<div class='deal_filter' align='center'><div class='submit' id='deal_good'>Успешно реализованные&nbsp;</div></div>"+
					"<div class='deal_filter' align='center'><div class='submit' id='deal_fail'>Нереализованные сделки&nbsp;</div></div>"+
				"</div>"+
				"<div style='float: left;'>"+
					"<input type='checkbox' id='event_1' checked class='deal_filter' /><span class='deal_checked_box status_1'> Первичный контакт</span><br>"+
					"<input type='checkbox' id='event_2' checked class='deal_filter' /><span class='deal_checked_box status_2'> Переговоры</span><br>"+
					"<input type='checkbox' id='event_3' checked class='deal_filter' /><span class='deal_checked_box status_3'> Принимают решение</span><br>"+
					"<input type='checkbox' id='event_4' checked class='deal_filter' /><span class='deal_checked_box status_4'> Согласование договора</span><br>"+
					"<input type='checkbox' id='event_5' class='deal_filter' /><span class='deal_checked_box status_5'> Успешно реализовано</span><br>"+
					"<input type='checkbox' id='event_6' class='deal_filter' /><span class='deal_checked_box status_6'> Закрыто и не реализовано</span><br>"+
				"</div>").animate( {opacity:1}, 500, "linear" ); } );
			//.animate( {opacity:0}, 500, "linear", function () {} ).animate( {opacity:1}, 500, "linear" );
			$('#after').hide("fast");
		}
		return false;
	})
	
	$("a#contact").on("click", function () {
		if ( prefix != "contact" ) {
			prefix = "contact";
			set_ajax(prefix);
			$("#filter").animate( {opacity:0}, 500, "linear", function(){ $(this).html(
				"<div style='display: block-inline;'>"+
					"<input type='submit' href='#open2' id='modal' id='add_contact' value='Добавить контакт' style='width: 240px; height: 30px;' /><br><br>"+
					"<div><input type='text' id='search' placeholder='Поиск контактов' style='width: 230px;' /></div><br>"+
					"<div class='submit' id='contact_all'>Все контакты</div>"+
					"<div class='submit' id='contact_until'>Контакты без задач</div>"+
					"<div class='submit' id='contact_without'>Контакты с просрочеными задачами</div><br>"+
				"</div>"+
				"<div style='display: block-inline;'>"+
					"<input checked class='0' type='checkbox'><span class='checkbox 0'>Сделка отсутствует</span><br>"+
					"<input checked class='1' type='checkbox'><span class='checkbox status_1'>Первичный контакт</span><br>"+
					"<input checked class='2' type='checkbox'><span class='checkbox status_2'>Переговоры</span><br>"+
					"<input checked class='3' type='checkbox'><span class='checkbox status_3'>Принимают решение</span><br>"+
					"<input checked class='4' type='checkbox'><span class='checkbox status_4'>Согласование договора</span><br>"+
					"<input class='5' type='checkbox'><span class='checkbox status_5'>Успешно реализовано</span><br>"+
					"<input class='6' type='checkbox'><span class='checkbox status_6'>Закрыто и нереализовано</span><br>"+
				"</div>").animate( {opacity:1}, 500, "linear" ); } );
			$('#after').hide("fast");
		}
		return false;
	})
	
	$("a#task").on("click", function () {
		if (prefix != 'task') {
			prefix = 'task';
			set_ajax(prefix);
			$("#filter").animate( {opacity:0}, 500, "linear", function(){ 
				$(this).html(
					'<div style="display: block-inline;">'+
						'<input type="submit" href="#open3" id="modal" style="width: 240px; height: 30px;" value="Добавить задачу" /><br><br>'+
						'<div class="submit" id="my_task">Мои задачи</div>'+
						'<div class="submit" id="my_none_task">Мои просроченные задачи</div>'+
						'<div class="submit" id="my_finish_task">Мои завершенные задачи</div>');
				if ( rang == '999' ) { $(this).append('<div class="submit" id="all_task">Все задачи</div>') };
				$(this).append('<br>'+
					'<select style="width: 110px;">'+
						'<option>За все время</option>'+
						'<option>За сегодня</option>'+
						'<option>За три дня</option>'+
						'<option>За неделю</option>'+
						'<option>За месяц</option>'+
						'<option>За полгода</option>'+
					'</select>'+
				"</div>"+
				"<div style='display: block-inline;'>"+
					'<div>Тип задачи</div>'+
						'<input type="checkbox" class="task_type" checked id="Звонок"/>Звонок<br>'+
						'<input type="checkbox" class="task_type" checked id="Встреча"/>Встреча<br>'+
						'<input type="checkbox" class="task_type" checked id="Письмо"/>Письмо<br><br>'+
					'<div>Статус задачи</div>'+
						'<input type="checkbox" class="task_status" checked id="none"/>Не завершенные<br>'+
						'<input type="checkbox" class="task_status" checked id="good"/>Завершенные<br>'+
				'</div>').animate( {opacity:1}, 500, "linear" ); } );
			$("#after").hide("fast");
		}
		return false;
	})

	$("#search").live("keypress", function (e) {
		if (e.which != 8) {
			search( $(this).val()+getChar(e) );
		} else {
			search( $(this).val() );
		}
		if (e.which == 27) {
			$("table").show();
			$(this).val("");
		}
	}).live('keyup', function (e) {
		if ( (e.keyCode == 27) || ($(this).val == '') ) { 
			$(this).val();
			$("table tr").show("fast");
		};
		if ( e.keyCode == 8 ) {
			search( $(this).val() );
		}
	});
	
	setInterval ( function () { var data='func=note'; $.ajax({ url: 'class/script.php', type: 'post', data: data, success: function (html) { $('article.article_task').html(html); } } ) }, 1000);
	
	$('.more_task').live('click', function () {
		// alert( $(this).attr('id') );
	})

	$('a#settings').live('click', function () {
		window.location.assign("http://new.ru/private/settings/");
	})
	
	$('a#exit').on('click', function () {
		window.location.replace("http://new.ru/private/unauth.php");
	})
	
	$('span#hide').live('click', function () {
		if ( !task_flag ) {
			$('.article_task').hide('fast');
			$(this).html('<strong>Show</strong>')
			task_flag = true;
		} else {
			$('.article_task').show('fast');
			$(this).html('<strong>Hide</strong>')
			task_flag = false;
		}
	}) 
	
	$('.more_task').live('click', function () {
		alert( $(this).attr('id') );
	})
	
	$('span#modal').live('click', function () {
		var id=$(this).attr('href');
		var maskH = $(document).height();
		var maskW = $(window).width();
		$('#mask').css({'width': maskW, 'height': maskH, 'display': 'block', 'background': '#aaa'});
		$('#mask').fadeIn(700);
		$('#mask').fadeTo('fast', 0.9);
		var winH = $(window).height();
		var winW = $(window).width();
		$(id).css({'top': winH/2 - $(id).height()/2});
		$(id).css({'left': winW/2 - $(id).width()/2});
		$(id).fadeIn(700);
		return false;
	});
})