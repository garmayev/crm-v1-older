<? 	session_start();
	if ( $_SESSION['rang'] != '999' ) { echo "<meta http-equiv='refresh' content='0;URL=http://new.ru'>"; exit; };
 ?>
<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		<title>*SHADES of GREY*</title>
		<meta charset="UTF-8" />
		<link href="../css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		<script type="text/javascript" src="js/_manager.js"></script>
	</head>
	<body>
		<header class="menu">
				<nav align="left">
					<ul style="float: left;">
						<li><a href="#" class="item" id="event">События</a> | </li>
						<li><a href="#" class="item" id="deal">Сделки</a> | </li>
						<li><a href="#" class="item" id="contact">Контакты</a> | </li>
						<li><a href="#" class="item" id="task">Задачи</a></li>
					<ul>
				</nav>
				<div>
					<a href="/"><div class="logo"></div></a>
				</div>
				<nav>
					<ul style="float: right;">
						<? 		if ( $_SESSION['rang'] == 999 ) {
									echo "<li><a href='#' class='item2' id='settings'>Настройки</a></li>";
									echo "<li>(<a href='#' class='item2' id='manag'>"; 
									echo $_SESSION['name'];
									echo "</a>)</li>";
								};
							 ?>
						<li><a href='#' class='item2' id='exit'>Выход</a></li>
					</ul>
				</nav>
			</div>
		</header>
		<section id="wrapper">
			<div id="table" style='width: 1000px; margin: 0 auto; text-align: left;'></div>
			<div id="add_user" style='width: 1000px; margin: 0 auto; text-align: left;'></div>
		</section>
	</body>
</html>