<?	session_start();
	include_once('../../class/_db.php');
 	class _manager {
		protected $DBCharset = "UTF-8";
		protected $DBDriver = "mysql";
		protected $DBHost = "localhost";
		protected $DBUser = "root";
		protected $DBName = "crm";
		protected $DBPass = "";
		protected $table = 'manager';
		protected static $dbh;
		
		public function __construct() {
			try {
				self::$dbh = new PDO($this->DBDriver.':'.$this->DBHost.';dbname='.$this->DBName.';charset='.$this->DBCharset.';', $this->DBUser, $SBPass);
				self::$dbh -> setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			} catch (PDOException $e) {
				echo "Error #1. ".$e->getMessage();
			}
		}
		
		public function show_manager() {
			$result = self::$dbh -> prepare("SELECT * FROM `".$this->DBName."`.`".$this->table."`;");
			$result -> execute();
			echo "	<table width=100% cellpadding=0 cellspacing=0 style='border: 1px solid #ccc;'>
						<thead>
							<tr>
								<th width=30></th>
								<th width=150>Имя Фамилия</th>
								<th width=150>e-mail</th>
								<th width=150>Изменения</th>
								<th width=150>Последний вход</th>
							</tr>
						</thead>";
			foreach ( $result->fetchAll() as $row ) {
				if ( $_SESSION['log'] != $row['id'] ) {
					echo "<tr id='".$row['id']."'><td width=30><input type='checkbox' /></td>";
					echo "<td width=150 id='fio'>";
					echo $row['fio'];
					echo "</td><td width=150>";
					echo $row['mail'];
					echo "</td><td width=150>";
					echo $row['change'];
					echo "</td><td width=150>";
					echo $row['last'];
					echo "</td><tr>";
				}
			}
			echo "</table>";
		}
		
		public function set_manager ($fio, $mail, $pass) {
			$date = date('Y-m-d');
			// $pass = md5($pass);
			$result = self::$dbh -> prepare("INSERT INTO `".$this->DBName."`.`".$this->table."` (`id`, `fio`, `mail`, `pass`, `rang`, `change`, `last`)"."\n"." VALUES (null, :fio, :mail, :pass, 111, '$date', '$date');");
			$result -> bindValue('fio', $fio, PDO::PARAM_STR);
			$result -> bindValue('mail', $mail, PDO::PARAM_STR);
			$result -> bindValue('pass', md5($pass), PDO::PARAM_STR);
			if ( !$result -> execute() ){ var_dump( $result->errorInfo() ); };
			// var_dump ($pass, md5($pass));
		}
		
		public function rang_manager($rang, $id) {
			$result = self::$dbh -> prepare("UPDATE `".$this->DBName."`.`".$this->table."` SET rang = :rang WHERE id=:id");
			$result -> bindValue('id', $id, PDO::PARAM_INT);
			switch ( $rang ) {
				case "a":
					$result -> bindValue('rang', (string)555, PDO::PARAM_INT);
				break;
				case "b": 
					$result -> bindValue('rang', (string)111, PDO::PARAM_INT);
				break;
			}
			$result -> execute();
		}
	}
?>