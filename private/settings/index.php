<? 	session_start();
	if ( ($_SESSION['rang'] == '999' ) && ($_SESSION['rang'] == '555') ) { echo "<meta http-equiv='refresh' content='0;URL=http://new.ru'>"; exit; };
 ?>
<!DOCTYPE html>
<html lang="RU-ru">
	<head>
		<title>Administrator freeman CRM</title>
		<meta charset="UTF-8" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<script type="text/javascript" src="../js/jquery-1.8.0.min.js"></script>
		<script type="text/javascript" src="js/script.js"></script>
		<script type="text/javascript" src="js/_manager.js"></script>
	</head>
	<body>
		<header class="menu">
				<nav align="left">
					<ul style="float: left; padding-left: 10px;">
						<li><a href="#" class="item" id="event">События</a> | </li>
						<li><a href="#" class="item" id="deal">Сделки</a> | </li>
						<li><a href="#" class="item" id="contact">Контакты</a> | </li>
						<li><a href="#" class="item" id="task">Задачи</a></li>
					<ul>
				</nav>
				<div>
					<a href="/"><div class="logo"></div></a>
				</div>
				<nav>
					<ul style="float: right;">
						<? 		if ( ($_SESSION['rang'] == 999) || ($_SESSION['rang'] == '555') ) {
									echo "<li>(<a href='#' class='item2' id='manag'>"; 
									echo $_SESSION['name'];
									echo "</a>)</li>";
								};
							 ?>
						<li><a href='#' class='item2' id='exit'>Выход</a></li>
					</ul>
				</nav>
			</div>
		</header>
		<section id="wrapper" style='width: 1000px; margin: 0 auto; text-align: left;'>
			<div id="contentright">
				<div id="table"></div>
				<br>
				<div id="after"></div>
				<div id="user" style='background: #fff; border: 1px solid #ccc; margin-top: 3px; padding: 10px; width: 275px; margin: 0 auto;'>
					<fieldset style='background: #fff; border: 1px solid #ccc; margin-top: 3px; width: 255px; margin: 0 auto;'>
						<legend>
							<strong>
								Новый менеджер
							</strong>
						</legend>
						<div id="add_user">
						</div>
					</fieldset>
				</div><br>
				<input type="button" href="#open1" id="add_manager" value="Добавить нового менеджера" style="float: right">
			</div>
		</section>
		<div id="box">
			<div id="open1" class="window">
				<input class="add" type="text" id="fio" placeholder="Имя, Фамилия" /><br>
				<input class="add" type="text" id="mail" placeholder="e-mail" /><br>
				<input class="add" type="password" id="pass" placeholder="Пароль" /><br>
				<input class="add" type="password" id="confirm_pass" placeholder="Повторите пароль" /><br>
				<input class="add" type="submit" id="add" /><br>
				<input class="add close" type='button' value='Отмена' />
				<a href='#' id='test'> test </a>
			</div>
			<div id="mask"></div>
		</div>
	</body>
</html>