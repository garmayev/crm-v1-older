$(function () {
	$('#after').hide();
	$('#user').hide();
	init();
	
	function show_manager() {
		$.ajax({
			url: 'class/script.php',
			type: 'post',
			data: 'func=show_manager',
			success: function (html) {
				$('#table').html(html);
			}
		})
	}
	
	function my_settings() {}
	
	function init() {
		show_manager();
		my_settings();
	}
	
	$('input[type=button]').live('click', function () {
		var id = $(this).attr('href');
		var maskH = $(document).height();
		var maskW = $(window).width();
		$('#mask').css({'width': maskW, 'height': maskH, 'display': 'block', 'background': '#aaa'});
		$('#mask').fadeIn(700);
		$('#mask').fadeTo('fast', 0.9);
		var winH = $(window).height();
		var winW = $(window).width();
		$(id).css({'top': winH/2 - $(id).height()/2});
		$(id).css({'left': winW/2 - $(id).width()/2});
		$(id).fadeIn(700);
		return false;
	})
	
	$('#mask, .close').live('click', function () {
		$('#mask').hide("fast");
		$('.window').hide("fast");
	})
	
	$('#add').live('click', function () {
		var flag = true;;
		if ( $('input#fio').val().length != 0 ) {
			if ( $('input#mail').val().length != 0 ) {
				if ( $('input#pass').val().length > 5 ) {
					if ( $('input#pass').val() == $('input#confirm_pass').val() ) {
						var data = "func=add_manager&fio="+$('input#fio').val()+"&mail="+$('input#mail').val()+"&pass="+$('input#pass').val();
					} else { 
						flag = false;
						alert('Heuston! We have got a problem! \nВведенные пароли не совпадают!\nПароль должен быть больше 6 символов');
					}
				} else {
					flag = false;
					alert('Heuston! We have got a problem! \nСлишком короткий пароль!'); 
				}
			} else {
				flag = false;
				alert('Heuston! We have got a problem! \nВведите адрес почтового ящика!');
			}
		} else {
			flag = false;
			alert('Heuston! We have got a problem! \nВведите Имя и Фамилию!');
		}
		if ( flag ) {
			$.ajax({
				url: 'class/script.php',
				type: 'post',
				data: data,
				success: function (html) {
					show_manager();
					alert(html);
				}
			})
		}
	})

	$("table tr").live("mouseenter", function () {
		$(this).not("thead tr").addClass("enter");
	});
	
	$("table tr").live("mouseleave", function () {
		$(this).not("thead tr").removeClass("enter");
	});
	
	$("table tr").live('click', function () {
		if ( $(this).not("thead tr").hasClass('select') ) { 
			$(this).not("thead tr").removeClass('select'); 
			$(this).not("thead tr").find("input[type=checkbox]").removeAttr('checked');
		} else { 
			$(this).not("thead tr").addClass('select');
			$(this).not("thead tr").find("input[type=checkbox]").attr('checked', 'checked');
		};
		if ( $("tr").not("thead tr").hasClass('select') ) { $('#after').html("<div>&nbsp;&nbsp;&nbsp;<a href='#' id='up'><strong>Возвести</strong> в статус <strong>'Помощник'</sctrong></a> | <a href='#' id='down'><strong>Убрать</strong> со статуса <strong>'Помощник'</strong></a><select id='live_man' style='display: none'></select>").show("fast"); } else { $('#after').hide('fast') }
	});
	
	$('#up').live('click', function () {
		$('table tr').find('input:checked').each( function () {
			$.ajax({url: 'class/script.php', type: 'post', data: 'func=rang_manager&state=a&id='+$(this).attr('id'), success: function (html) { alert(html); }});
		});
		return false;
	})
	
	$('#down').live('click', function () {
		$('table tr').find('input:checked').each( function () {
			$.ajax({url: 'class/script.php', type: 'post', data: 'func=rang_manager&state=b&id='+$(this).attr('id'), success: function (html) { alert(html); }});
		});
		return false;
	})

	$('a#main').on('click', function () {
		window.location.assign("http://new.ru/");
	})
	
	$('a#manag').live('click', function () {
		// window.location.assign("http://new.ru/private/manag/")
	})
	
	$('a#exit').on('click', function () {
		window.location.replace("http://new.ru/private/unauth.php");
	})
	
	$('a#test').on('click', function () {
		$.ajax({url: 'class/script.php', type: 'post', data: 'func=md5test&id='+$('input#pass').val(), success: function (html) { alert(html); }});
	})
})